# nacos
![输入图片说明](image.png)
#### 介绍
阿里 Nacos 注册中心与配置中心，方便同行下载安装介质。
1. 官方代码库：https://github.com/alibaba/nacos 
2. 官方文档库：https://nacos.io/

#### 软件架构
![nacos架构](nacosimage.png)


#### 安装教程

单机模式下运行Nacos
Linux/Unix/Mac

```
$ sh startup.sh -m standalone
```
Windows

```
$ cmd startup.cmd -m standalone
```

#### 使用说明


1.安装数据库，版本要求：5.6.5+
2.初始化mysql数据库，数据库初始化文件：mysql-schema.sql
3.修改conf/application.properties文件，增加支持mysql数据源配置（目前只支持mysql），添加mysql数据源的url、用户名和密码。

```
spring.datasource.platform=mysql

db.num=1
db.url.0=jdbc:mysql://11.162.196.16:3306/nacos_devtest?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=nacos_devtest
db.password=youdontknow
```

其他详见官方文档。

